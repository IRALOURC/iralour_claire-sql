CREATE TABLE Clients
(
    ChambreId INT PRIMARY KEY NOT NULL -- ON retrouve chambre id dans plusieurs tables
    FirstName VARCHAR NOT NULL,
    LastName  VARCHAR NOT NULL,           
    DateDebut DATE NOT NULL,
    DateFin DATE NOT NULL,
    Paiement varchar(20) not null, -- moyen de paiement
    Contact varchar(20) not null,
    Total INT not null,
    FOREIGN KEY (ChambreId) REFERENCES Chambres(ChambreId), -- reference a la table Chambre
    FOREIGN KEY (Total) REFERENCES Chambres(PrixC) + RestaurationPrix(PrixR) + Services(PrixS) -- reference aux tables aec tous les prix
)
CREATE TABLE Chambres
(
    ChambreId INT PRIMARY KEY NOT NULL,
    TypeId varchar(20) not null,
    Lit varchar(20) not null,
    PrixC INT not null
    FOREIGN KEY (ChambreId) REFERENCES Reservation(ChambreId), -- reference a la table Reservation

)
CREATE TABLE Reservation
(
    ChambreId INT not null
    Disponibilite binary not null, -- oui ou non
    
)
CREATE TABLE Restauration
(
    ChambreId INT PRIMARY KEY not null,
    MenuId INT not null,
    FOREIGN KEY (MenuId) REFERENCES RestaurationPrix(MenuId)

)
CREATE TABLE RestaurationPrix
(
    MenuId binary not null,
    Repas varchar(50) not null,
    PrixR INT not null

)
CREATE TABLE Services
(
    ChambreId INT PRIMARY KEY NOT NULL,
    Parking binary not null,
    Spa binary not null,
    Menage binary not null,
    PrixS INT NOT NULL
    FOREIGN KEY (PrixS) REFERENCES ServicesPrix(PrixS1 + PrixS2 + PrixS3)
)
CREATE TABLE ServicesPrix
(
    Parking binary not null
    PrixS1 INT NOT NULL
    Spa binary not null
    PrixS2 INT NOT NULL
    Menage binary not null
    PrixS3 INT NOT NULL

)


