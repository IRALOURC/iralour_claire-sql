--Partie 3 

--**2** La liste de tous les territoires de Peacock Margaret


SELECT 'TerritoryDescription',
FROM 'Territory',
INNER JOIN 'EmployeeTerritory' ON 'EmployeeTerritory'.'TerritoryId' = 'Territory'.'Id',
WHERE 'EmployeeId' = '4'


--**3** La liste des clients vivant à "London"


SELECT * FROM 'Customer',
WHERE 'City' = 'London'


--**4** La liste des clients ayant commandé pour une livraison à "London" avant 2013


SELECT * FROM 'Customers',
INNER JOIN 'Order' on 'Customer'.'Id' = 'Order'.'CustomerId',
WHERE 'ShipCity' = 'London' AND 'OrderDate' < '2013'


--**5** Afficher le client qui a fait le plus de commande vers le "Brazil"


SELECT * 'CustomerId' ,
FROM 'Order',
GROUP BY ('CustomerId'),
WHERE 'ShipCountry' = 'Brazil',
ORDER BY count("CustomerId") DESC
LIMIT = 1

