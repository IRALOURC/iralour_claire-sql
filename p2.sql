

INSERT INTO 'Clients' ('ChambreId','FirstName','LastName','DateDebut','DateFin','Paiement','Contact','Total') VALUES('1','Christina','George','21/06/21','28/06/21','Especes','0102030405','10,21$');
INSERT INTO 'Clients' ('ChambreId','FirstName','LastName','DateDebut','DateFin','Paiement','Contact','Total') VALUES('2','Marie','Silva','22/06/21','28/06/21','Carte Bancaire','0201030405','22,98$');
INSERT INTO 'Clients' ('ChambreId','FirstName','LastName','DateDebut','DateFin','Paiement','Contact','Total') VALUES('3','Karen','Mendoza','21/06/21','26/06/21','Especes','0301020405','28,67');
INSERT INTO 'Clients' ('ChambreId','FirstName','LastName','DateDebut','DateFin','Paiement','Contact','Total') VALUES('5','Katrina','Torres','23/06/21','30/06/21','Especes','0401020305','26,45$');
INSERT INTO 'Clients' ('ChambreId','FirstName','LastName','DateDebut','DateFin','Paiement','Contact','Total') VALUES('6','James','Alvarado','26/06/21','27/06/21','Cheques','0501020304','23,78$');
INSERT INTO 'Clients' ('ChambreId','FirstName','LastName','DateDebut','DateFin','Paiement','Contact','Total') VALUES('8','Zachary','Lowe','24/06/21','29/06/21','Carte Bancaire','0302010504','37,03$');
INSERT INTO 'Clients' ('ChambreId','FirstName','LastName','DateDebut','DateFin','Paiement','Contact','Total') VALUES('9','Brandon','Black','20/06/21','24/06/21','Carte Bancaire','0504030201','32,33$');
INSERT INTO 'Clients' ('ChambreId','FirstName','LastName','DateDebut','DateFin','Paiement','Contact','Total') VALUES('10','Joel','Armstrong','25/06/21','27/06/21','Especes','0205030401','20.34$');

-- Pour le total on calcule le prix de tous les services x le nombre de jour de reservation
--Chambre 1 30 + 10 + 30 = 70 , 70 x 7 = 490$
--Chambre 2 15 + 10 + 40 = 65 , 65 X 6 = 390$
--Chambre 3 5 + 15 + 20 = 40 , 40 x 5 = 200$
--Chambre 5 5 + 20 + 30 = 55 , 55 x 7 = 385$
--Chambre 6 25 + 15 + 25 = 65 , 65 x 1 = 65$
--Chambre 8 15 + 20 + 10 = 45 , 45 x 5 = 225$
--Chambre 9 30 + 10 + 40 = 80 , 80 x 4 = 320$
--Chambre 10 0 + 20 + 40 = 60 , 60 x 2 = 120$

INSERT INTO 'Chambres' ('ChambreId','TypeId','Lit','PrixC') VALUES('1','Suite','Simple','30$');
INSERT INTO 'Chambres' ('ChambreId','TypeId','Lit','PrixC') VALUES('2','Suite','Double','40$');
INSERT INTO 'Chambres' ('ChambreId','TypeId','Lit','PrixC') VALUES('3','Standard','Simple','20$');
INSERT INTO 'Chambres' ('ChambreId','TypeId','Lit','PrixC') VALUES('4','Economie','Simple','10$');
INSERT INTO 'Chambres' ('ChambreId','TypeId','Lit','PrixC') VALUES('5','Suite','Simple','30$');
INSERT INTO 'Chambres' ('ChambreId','TypeId','Lit','PrixC') VALUES('6','Standard','Double','25$');
INSERT INTO 'Chambres' ('ChambreId','TypeId','Lit','PrixC') VALUES('7','Standard','Double','25$');
INSERT INTO 'Chambres' ('ChambreId','TypeId','Lit','PrixC') VALUES('8','Economie','Simple','10$');
INSERT INTO 'Chambres' ('ChambreId','TypeId','Lit','PrixC') VALUES('9','Suite','Double','40$');
INSERT INTO 'Chambres' ('ChambreId','TypeId','Lit','PrixC') VALUES('10','Suite','Double','40$');

INSERT INTO 'Reservation' ('ChambreId','Disponibilite') VALUES('1','oui');
INSERT INTO 'Reservation' ('ChambreId','Disponibilite') VALUES('2','oui');
INSERT INTO 'Reservation' ('ChambreId','Disponibilite') VALUES('3','oui');
INSERT INTO 'Reservation' ('ChambreId','Disponibilite') VALUES('4','non');
INSERT INTO 'Reservation' ('ChambreId','Disponibilite') VALUES('5','oui');
INSERT INTO 'Reservation' ('ChambreId','Disponibilite') VALUES('6','oui');
INSERT INTO 'Reservation' ('ChambreId','Disponibilite') VALUES('7','non');
INSERT INTO 'Reservation' ('ChambreId','Disponibilite') VALUES('8','oui');
INSERT INTO 'Reservation' ('ChambreId','Disponibilite') VALUES('9','oui');
INSERT INTO 'Reservation' ('ChambreId','Disponibilite') VALUES('10','oui');

INSERT INTO 'Restauration' ('ChambreId','MenuId') VALUES('1','M1');
INSERT INTO 'Restauration' ('ChambreId','MenuId') VALUES('2','M1');
INSERT INTO 'Restauration' ('ChambreId','MenuId') VALUES('3','M2');
INSERT INTO 'Restauration' ('ChambreId','MenuId') VALUES('5','M3');
INSERT INTO 'Restauration' ('ChambreId','MenuId') VALUES('6','M2');
INSERT INTO 'Restauration' ('ChambreId','MenuId') VALUES('8','M3');
INSERT INTO 'Restauration' ('ChambreId','MenuId') VALUES('9','M1');
INSERT INTO 'Restauration' ('ChambreId','MenuId') VALUES('10','M3');

INSERT INTO 'RestaurationPrix' ('MenuId','Repas','PrixR') VALUES('M1','Salade-Frite-Agneau-Chocolat,10$');
INSERT INTO 'RestaurationPrix' ('MenuId','Repas','PrixR') VALUES('M2','Salade-Poisson-Legumes-Glaces,15$');
INSERT INTO 'RestaurationPrix' ('MenuId','Repas','PrixR') VALUES('M3','Saumon-Plat du chef-Tarte Citron,20$');

INSERT INTO 'Services' ('ChambreId','Parking','Spa','Menage','PrixS') VALUES('1','oui','oui','oui','30$');
INSERT INTO 'Services' ('ChambreId','Parking','Spa','Menage','PrixS') VALUES('2','oui','non','oui','15$');
INSERT INTO 'Services' ('ChambreId','Parking','Spa','Menage','PrixS') VALUES('3','non','non','oui','5$');
INSERT INTO 'Services' ('ChambreId','Parking','Spa','Menage','PrixS') VALUES('5','non','non','oui','5$');
INSERT INTO 'Services' ('ChambreId','Parking','Spa','Menage','PrixS') VALUES('6','oui','oui','non','25$');
INSERT INTO 'Services' ('ChambreId','Parking','Spa','Menage','PrixS') VALUES('8','non','oui','non','15$');
INSERT INTO 'Services' ('ChambreId','Parking','Spa','Menage','PrixS') VALUES('9','oui','oui','oui','30$');
INSERT INTO 'Services' ('ChambreId','Parking','Spa','Menage','PrixS') VALUES('10','non','non','non','0$');

INSERT INTO 'ServicesPrix' ('Parking','PrixS1') VALUES('Prix','10$');
INSERT INTO 'ServicesPrix' ('Spa','PrixS2') VALUES('Prix','15$');
INSERT INTO 'ServicesPrix' ('Menage','PrixS3') VALUES('Prix','5$');
